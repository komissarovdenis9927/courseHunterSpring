package com.coursehunter.spring.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@RestController
@RequestMapping("/hello")
public class HelloController {


    @Value("#{T(java.lang.Math).random() * 100.0}")
    private int randomInt;
    private String companyName;
    MessageSource messageSource;

    public HelloController(@Value("${company.name}") String companyName, MessageSource messageSource){
        this.companyName = companyName;
        this.randomInt = randomInt;
        this.messageSource = messageSource;
    }

    @GetMapping
    public ResponseEntity getHello(@RequestParam("name") String name, @RequestParam("locale") String localeLang){
        Locale locale = new Locale(localeLang);
        return new ResponseEntity(messageSource.getMessage("user.message", new String[] {companyName + randomInt, name}, locale), HttpStatus.OK);
    }

}
